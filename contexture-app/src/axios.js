import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(VueAxios, axios)

// Vue.prototype.$http = axios

Vue.config.productionTip = false

const API_URL = process.env.API_URL || 'http://dev1.contexture.ai/services'

export default axios.create({
  baseURL: API_URL,
  headers: {
    'Content-Type': 'application/json',
    // 'Authorization': 'Bearer ' + localStorage.token
    'Authorization': 'Bearer eyJraWQiOiJrMSIsImFsZyI6IlJTMjU2In0.eyJpc3MiOiJodHRwczovL2NvbnRleHR1cmUuYWkiLCJhdWQiOiJodHRwczovL2NvbnRleHR1cmUuYWkiLCJleHAiOjE1NTYwNzk0NDgsImp0aSI6InZJeXZWX2h0T05vZWpVemhub3lfS1EiLCJpYXQiOjE1NTU0NzQ2NDgsIm5iZiI6MTU1NTQ3NDUyOCwic3ViIjoidHJvbWJvbmVyZEBnbWFpbC5jb20ifQ.YvXMxW7TMDXp-PkDDtyLtZ4itNjoHoL-h1QViZ6s95GFIHXe4KlDQMaa4iM7tAsyTBlmlTdCenHCSMT3nuJAAbbESw-HYDg4OaHeIWkvlc3CKH2aRovx-0EYpWE1B5DRpEmKeO0-QlSxOUNO7rh-rhW7Da5TVB5Vd9CxDz2VNCxJduNlcrUKbGPsxjaobpWXK8ALWCDH4Teey5JI7QDfDM5QxIwfwksGLLzRoQUF0VSpkn0rLn1HPvTTIhdy0D7z84GKk_mqIGjerPEOWlVTHNQdETIdt2sI_HZb6s2wmbH8ZC5_G0suY3yOMMnVOLYGjGgSK-nB7IgcC9E_nWqXeg',
    'Access-Control-Allow-Origin': '*',
    'Accept' : 'application/json, text/plain, */*',
    'Access-Control-Allow-Methods' : 'GET, PUT, POST, DELETE, OPTIONS',
    'Access-Control-Allow-Credentials' : true
  }
})